window.onload = getMessages()

const btn = document.querySelector('#submit')
const form = document.querySelector('#emailform')
const commentSection = document.querySelector('.comment-section')
const url = 'http://127.0.0.1:8000'

function delay(ms) {
    return new Promise(res => setTimeout(res, ms))
}

function stringifyFormData() {
    const formData = new FormData(form)
    const object = {}
    formData.forEach((value, key) => {
        object[key] = value
    })
    return JSON.stringify(object)
}

function postMessage() {
    return fetch(`${url}/v1/postmessage`, {
        method: 'POST',
        headers: {
            'Accept': 'appplication/json',
            'Content-Type': 'application/json'
        },
        body: stringifyFormData(),
    })
}

async function getMessages() {
    await delay(10)
    commentSection.innerHTML = ''
    return fetch(`${url}/data`)
        .then(res => res.json())
        .then((data) => {
            for (let i = 0; i < data.length; i++) {
                const node = document.createElement('li')
                node.id = data[i].id
                node.innerText=`Name: ${data[i].name}\n Message: ${data[i].message}`
                commentSection.prepend(node)
            }
        }
    )
}

btn.addEventListener('click', async (e) => {
    e.preventDefault()
    await Promise.all([postMessage(), getMessages()])
})
