const express = require('express')
const path = require('path')
const cors = require('cors')
const bodyParser = require('body-parser')
const { nanoid } = require('nanoid')
const db = require('./db/sqlite')

const app = express()

app.use(cors({
    origin: true,
    credentials: true
}))

app.use(bodyParser.urlencoded({ extended: false}))
app.use(express.static(path.join(__dirname, "public")))
app.use(express.json())

const route = express.Router()
const port = process.env.PORT || 8000

app.use('/v1', route)

route.get('/', (req, res) => {
    res.sendFile(__dirname + '/index.html')
})

app.get('/data', async (req, res) => {
    await db.all('SELECT * FROM peoples;', [], function(err, rows) {
        return res.send(rows)
    })
})

route.post('/postmessage', (req, res) => {
    const {name, body} = req.body
    const id = nanoid()
    db.run(`INSERT INTO peoples(id, name, message) VALUES(?,?,?)`, [id, name, body],
        function(err) {
            if (err) return console.log(err.message);
            console.log(`successful post!`);
    })
})

app.listen(port, () => {
    console.log(`Serving sqlite database as JSON on port: ${port}`)
})
